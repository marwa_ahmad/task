﻿using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public static class ListExtension
    {       
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return (enumerable == null) || (enumerable.Any() == false);
        }
    }
}
