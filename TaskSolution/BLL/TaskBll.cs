﻿using System;
using System.Collections.Generic;
using Common;
using Model;
using DAL.Contracts;
using BLL.Contracts;
using DAL;
namespace BLL
{
    public class TaskBll<T> : ITaskBll<T> where T : Task
    {
        private ITaskDAL<T> _taskDAL;
        public TaskBll()
        {
            _taskDAL = new TaskDAL<T>();
        }

        public void AddTask(T task)
        {
            task.ID = Guid.NewGuid().ToString("N");
            _taskDAL.Create(task);
        }
        public List<T> GetAll()
        {
            var tasks = _taskDAL.GetAll();
            return tasks.IsNullOrEmpty() ? null : tasks;
        }

        public List<T> GetSubTasks(string taskId)
        {
           var subTasks = _taskDAL.GetSubTasks(taskId);
           return subTasks.IsNullOrEmpty() ? null : subTasks;
        }

        public void AddSubTask(string parentId, T childTask)
        {
            childTask.ID = Guid.NewGuid().ToString("N");
            childTask.ParentID = parentId;
            _taskDAL.Create(childTask);
        }
    }
}
