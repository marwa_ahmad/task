﻿using System.Collections.Generic;

namespace BLL.Contracts
{
    public interface ITaskBll<T>
    {
        void AddTask(T task);
        List<T> GetAll();
        List<T> GetSubTasks(string taskId);
        void AddSubTask(string taskId, T childTask);
    }
}
