﻿using System.Collections.Generic;

namespace DAL.Contracts
{
    public interface ITaskDAL<T>
    {
        void Create(T task);
        List<T> GetAll();
        
        List<T> GetSubTasks(string taskId);
    }
}
