﻿using System.Web.Mvc;
using BLL;
using BLL.Contracts;
using Model;
using Portal.Utilities;
using Portal.ViewModel;

namespace Portal.Controllers
{
    public class TaskController : Controller
    {
        private static readonly ITaskBll<Task> _TASKBLL = new TaskBll<Task>(); 
        
        public ActionResult GetAll()
        {
            var tasks = _TASKBLL.GetAll();
            if (tasks == null) return View();
            var tasksGetVms = tasks.ToTaskGetVMList();
            return View(tasksGetVms);
        }        
        
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TaskCVM model)
        {
            try
            {
                if (ModelState.IsValid == false) return View(model);
                _TASKBLL.AddTask(model.ToTask());
                return RedirectToAction("GetAll");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Details(string taskId)
        {
            if (string.IsNullOrWhiteSpace(taskId)) RedirectToAction("GetAll");
            var subTasks = _TASKBLL.GetSubTasks(taskId);
            if (subTasks == null) return View();
            var tasksGetVms = subTasks.ToTaskGetVMList();
            return View(tasksGetVms);            
        }

        public ActionResult CreateSubtask(string taskId)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSubtask(string taskId, TaskCVM model)
        {
            try
            {
                if (ModelState.IsValid == false) return View(model);
                _TASKBLL.AddSubTask(taskId, model.ToTask());
                return RedirectToAction("Details", new {taskId = taskId});
            }
            catch
            {
                return View();
            }
        }
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
