﻿using System;
using Model.Enums;

namespace Portal.ViewModel
{
    public class TaskGetVM
    {
        private string _id;
        private String _title;
        private SeverityType _severity;
        private PriorityType _priority;
        private DateTime _date;        
        private StatusType _status;

        public TaskGetVM(string id, string title, SeverityType severityType,
            PriorityType priorityType, DateTime date, StatusType statusType)
        {
            _id = id;
            _title = title;
            _severity = severityType;
            _priority = priorityType;
            _date = date;            
            _status = statusType;
        }
        public string ID  { get { return _id; } }
        public String Title { get { return _title; } }
        public SeverityType Severity { get { return _severity; } }
        public PriorityType Priority { get { return _priority; } }
        public DateTime Date { get { return _date; } }        
        public StatusType Status { get { return _status; } }
    }
}