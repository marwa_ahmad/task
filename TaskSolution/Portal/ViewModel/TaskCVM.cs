﻿using System;
using System.ComponentModel.DataAnnotations;
using Model.Enums;
using Portal.Utilities;

namespace Portal.ViewModel
{
    public class TaskCVM
    {        
        [Required(ErrorMessage = Messages.TitleRequired)]
        public String Title { get; set; }

        [Required(ErrorMessage = Messages.SeverityRequired)]
        public SeverityType Severity { get; set; }

        [Required(ErrorMessage = Messages.PriorityRequired)]
        public PriorityType Priority { get; set; }

        [Required(ErrorMessage = Messages.DateRequired)]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = Messages.StatusRequired)]
        public StatusType Status { get; set; }

        [DataType(DataType.MultilineText)]
        public String Description { get; set; }
       
    }
}