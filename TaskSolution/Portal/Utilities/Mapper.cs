﻿using System.Collections.Generic;
using System.Linq;
using Model;
using Portal.ViewModel;

namespace Portal.Utilities
{
    public static class Mapper
    {
        public static TaskGetVM ToTaskGetVM(this Task task)
        {
            return new TaskGetVM(task.ID, task.Title, task.Severity, task.Priority, task.Date, task.Status);
        }

        public static List<TaskGetVM> ToTaskGetVMList(this List<Task> taskList)
        {
            return taskList.Select(task => task.ToTaskGetVM()).ToList();
        }

        public static Task ToTask(this TaskCVM taskCVM)
        {
            return new Task()
            {
                Date = taskCVM.Date,
                Priority = taskCVM.Priority,
                Severity = taskCVM.Severity,
                Title = taskCVM.Title,
                Status = taskCVM.Status
            };
        }
    }
}