﻿

namespace Portal.Utilities
{
    public static class Messages
    {
        public const string TitleRequired = "Title is required";
        public const string SeverityRequired = "Serevrity is required";
        public const string PriorityRequired = "Priority is required";
        public const string DateRequired = "Date is required";
        public const string StatusRequired = "Status is required";
    }
}