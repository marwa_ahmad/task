﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Contracts;
using Model;
using Common;

namespace DAL
{
    public class TaskDAL<T> : ITaskDAL<T> where T : Task
    {
        private TaskContainer<T> _taskContainer;
        public TaskDAL()
        {
            _taskContainer = new TaskContainer<T>();
            _taskContainer.Tasks = new List<T>();
        }


        public void Create(T task)
        {
            _taskContainer.Tasks.Add(task);
        }

        public List<T> GetAll()
        {
            return _taskContainer.Tasks;
        }

        public void EditTask(T task)
        {
            //if (task == null) throw new ArgumentNullException();
            var matchedTask = _taskContainer.Tasks.FirstOrDefault(t => String.CompareOrdinal(t.ID, task.ID) == 0);
            if (matchedTask == null) throw new KeyNotFoundException();
            var taskParentID = matchedTask.ParentID;
            matchedTask = task;
            matchedTask.ParentID = taskParentID;
        }
        public List<T> GetSubTasks(string taskId)
        {
            //if (string.IsNullOrWhiteSpace(taskId)) return null;
            var tasks = _taskContainer.Tasks.Where(t => String.CompareOrdinal(t.ParentID, taskId) == 0);
            return tasks.IsNullOrEmpty() ? null : tasks.ToList<T>();
        }
    }
}
