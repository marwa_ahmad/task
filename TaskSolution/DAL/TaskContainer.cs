﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class TaskContainer<T>
    {
        public List<T> Tasks { get; set; }
    }
}
