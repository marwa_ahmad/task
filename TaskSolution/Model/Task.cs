﻿using System;
using Model.Enums;

namespace Model
{
    public class Task
    {
        public string ID { get; set; }
        public String Title { get; set; }
        public SeverityType Severity { get; set; }
        public PriorityType Priority { get; set; }
        public DateTime Date { get; set; }        
        public StatusType Status { get; set; }
        public string ParentID { get; set; }

    }
}
