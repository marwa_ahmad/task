﻿
namespace Model.Enums
{
    /// <summary>
    /// These enums values should not be changed; as values persisted in DB
    /// </summary>

    public enum SeverityType
    { 
        Low = 0,
        Medium = 1, 
        High = 2
    }
    public enum PriorityType
    {
        Low = 0,
        Medium = 1,
        High = 2
    }

    public enum StatusType
    {
        Backlog = 0,
        Inprogress = 1,
        Done = 2
    }
}
